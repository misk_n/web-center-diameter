# include <vector>
# include <stack>

# include "graph.hpp"
# include "Igraph.hpp"

namespace diameter
{
    using vector_t = std::vector<node_t>;

    class cpu_graph
            : public Igraph
                    , public graph<node_t, vector_t, raw_arc_vector_t>
    {
    public:
        cpu_graph();

        using node = node_t;

        struct bfs_status perform_bfs(node_t start) override;
        void build(raw_arc_vector_t&, node_t) override;
        node_t get_node_count() const override;

        void compute_scc(std::vector<node_t>& scc) override ;

        std::vector<node_t> get_adjacent(node_t n) override;
    };

    Igraph *Igraph_factory();
}

class Loader
{
public:
    Loader()
    {
        factory = diameter::Igraph_factory;
    }
};

static Loader ldr;
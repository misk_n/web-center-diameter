# include "bfs.hh"

namespace diameter
{
    struct bfs_status cpu_graph::perform_bfs(cpu_graph::node start)
    {
        auto res = bfs_status();
        res.last_frontier = {start};
        return res;
    }

    cpu_graph::cpu_graph()
    {
        return;
    }

    void
    cpu_graph::build(raw_arc_vector_t& arcs, node_t nb_nodes)
    {
        graph<node_t, vector_t, raw_arc_vector_t>::build(arcs, nb_nodes);
    }

    node_t cpu_graph::get_node_count() const
    {
        return get_nb_nodes();
    }

    // Tarjan's algorithm
    // TODO: GPU version
    void cpu_graph::compute_scc(std::vector<node_t> &scc)
    {
        int num_visit = 0, num_scc = 0;
        std::vector<node_t> ord(get_node_count(), -1);
        std::vector<node_t> low(get_node_count());
        std::vector<bool> in(get_node_count(), false);
        std::stack<node_t> s;
        std::stack<std::pair<node_t, node_t> > dfs;

        for (int i = 0; i < get_node_count(); i++) {
            if (ord[i] != -1) continue;

            dfs.push(std::make_pair(i, -1));

            while (!dfs.empty()) {
                node_t v = dfs.top().first;
                node_t index = dfs.top().second;

                dfs.pop();

                if (index == -1) {
                    ord[v] = low[v] = num_visit++;
                    s.push(v);
                    in[v] = true;
                } else {
                    low[v] = std::min(low[v], low[get_adjacent(v)[index]]);
                }

                for (index++; index < get_adjacent(v).size(); index++) {
                    node_t w = get_adjacent(v)[index];

                    if (ord[w] == -1) {
                        dfs.push(std::make_pair(v, index));
                        dfs.push(std::make_pair(w, -1));
                        break;
                    } else if (in[w] == true) {
                        low[v] = std::min(low[v], ord[w]);
                    }
                }

                if (index == get_adjacent(v).size() && low[v] == ord[v]) {
                    while (true) {
                        node_t w = s.top();

                        s.pop();
                        in[w] = false;
                        scc[w] = num_scc;

                        if (v == w) break;
                    }

                    num_scc++;
                }
            }
        }
    }

    std::vector<node_t> cpu_graph::get_adjacent(node_t n) {
        return graph::get_adjacent(n);
    }

    Igraph *Igraph_factory()
    {
        return new diameter::cpu_graph();
    }

}

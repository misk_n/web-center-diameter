# include <algorithm>

# include <thrust/device_vector.h>
# include <device_launch_parameters.h>

# include "Igraph.hpp"
# include "graph.hpp"

namespace diameter
{
  using vector_t = thrust::device_vector<node_t>;
  using raw_arc_vector_t = std::vector<std::pair<node_t, node_t>>;

  class cuda_graph
    : public Igraph
    , public graph<node_t, vector_t, raw_arc_vector_t>
  {
  public:
    using node = node_t;

    struct bfs_status perform_bfs(node_t start) override;
    void build(raw_arc_vector_t&, node_t) override;
      node_t get_node_count() const override;
  };

    Igraph *Igraph_factory();
}

class Loader
{
public:
    Loader()
    {
      factory = diameter::Igraph_factory;
    }
};

static Loader ldr;

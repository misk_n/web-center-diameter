# include "bfs.cuh"

using graph_t = diameter::cuda_graph;

__global__
void cuda_bfs_kernel(graph_t::node* src,
                     graph_t::node* dst,
                     bool *frontier,
                     bool *visited,
                     graph_t::node* distances,
                     graph_t::node nb_nodes,
                     bool *not_empty)
{
    graph_t::node id = blockIdx.x * 1024 + threadIdx.x;
    graph_t::node start;
    if (!id)
        start = 0;
    else
        start = src[id - 1];
    graph_t::node end = src[id];

    if(id < nb_nodes && frontier[id])
    {
        frontier[id] = false;
        visited[id] = true;
        for(graph_t::node i = start; i < end; i++)
        {
            graph_t::node dst_id = dst[i];
            if(!visited[dst_id])
            {
                distances[dst_id] = distances[id] + 1;
                frontier[dst_id] = true;
                *not_empty = true;
            }
        }
    }
}

namespace diameter
{
  
  Igraph *Igraph_factory()
  {
    return new cuda_graph();
  }

  void cuda_graph::build(raw_arc_vector_t& arcs, node_t nb_nodes)
  {
    graph<node_t, vector_t, raw_arc_vector_t>::build(arcs, nb_nodes);
  }

    node_t cuda_graph::get_node_count() const {
        return get_nb_nodes();
    }

  void cuda_graph::perform_bfs(cuda_graph::node start)
  {
    auto& graph = *this;
    thrust::device_vector<bool> frontier(graph.get_nb_nodes(), false);
    frontier[start] = true;
    thrust::device_vector<bool> visited(graph.get_nb_nodes(), false);
    thrust::device_vector<graph_t::node> distances(graph.get_nb_nodes(), -1);
    distances[start] = 0;
    auto d_not_empty = thrust::device_malloc<bool>(sizeof (bool));

    for (*d_not_empty = true; *d_not_empty;)
    {
      std::cout << "continue: " << *d_not_empty << std::endl;
      *d_not_empty = false;
      cuda_bfs_kernel <<< graph.get_nb_nodes() / 1024 + 1, 1024 >>>
        (thrust::raw_pointer_cast(graph.get_sources().data()),
         thrust::raw_pointer_cast(graph.get_destinations().data()),
         thrust::raw_pointer_cast(frontier.data()),
         thrust::raw_pointer_cast(visited.data()),
         thrust::raw_pointer_cast(distances.data()),
         graph.get_nb_nodes(), d_not_empty.get());

      auto print_el = [](const auto& n) { std::cout << n << " "; };
      auto print = [print_el](const auto& v) {
        std::for_each(v.begin(), v.end(), print_el);
        std::cout << std::endl;
      };

      std::cout << "visited:" << std::endl;
      print(visited);
      std::cout << "frontier:" << std::endl;
      print(frontier);
      std::cout << "distances:" << std::endl;
      print(distances);
    }
  }
}

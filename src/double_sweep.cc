//
// Created by nicolas on 09/06/18.
//

# include "Igraph.hpp"

namespace diameter
{
    static node_t GetRandom(node_t nb_nodes) {
        static long long unsigned x = 123456789;
        static long long unsigned y = 362436039;
        static long long unsigned z = 521288629;
        static long long unsigned w = 88675123;
        unsigned long long t;

        t = x ^ (x << 11);
        x = y;
        y = z;
        z = w;
        w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));

        return w % nb_nodes;
    }

    node_t double_sweep(unsigned num_double_sweep, Igraph& graph, Igraph& rgraph)
    {
        node_t diameter = -1;
        // distribuable
        for (unsigned i = 0; i < num_double_sweep; i++)
        {
            node_t start = GetRandom(graph.get_node_count());

            // forward BFS
            auto prep_status = graph.perform_bfs(start);

            // barkward BFS
            auto bfs_status = rgraph.perform_bfs(prep_status.last_frontier[0]); // TODO: random index

            diameter = std::max(diameter, bfs_status.max_distance);
        }
        return diameter;
    }
}
//
// Created by Nicolas Misk on 5/19/18.
//

#include <dlfcn.h>

#include "Igraph.hpp"

factory_t graph_factory;

void main()
{
  // TODO : dlopen... etc.
  void *handle = dlopen("libbfs.so", RTLD_NOW);
  if(handle == nullptr)
    throw std::runtime_error("Unable to dlopen libfs.so\n dlerror(): " + dlerror());
  std::vector<std::pair<long long int, long long int> > arcs;
  arcs.push_back(std::make_pair(0, 1));
  arcs.push_back(std::make_pair(1, 2));
  arcs.push_back(std::make_pair(2, 0));
  arcs.push_back(std::make_pair(2, 3));
  arcs.push_back(std::make_pair(3, 1));
  arcs.push_back(std::make_pair(3, 4));
  arcs.push_back(std::make_pair(4, 2));

Igraph* gr = buildIgraph();
  //gpu_graph<long long int> gr(arcs, 5);

    //auto sources = gr.get_sources();
    //for (auto it = sources.begin(); it != sources.end(); it++)
    //    std::cout << *it << std::endl;

    //std::cout << std::endl;

    //auto destinations = gr.get_destinations();
    //for (auto it = destinations.begin(); it != destinations.end(); it++)
    //    std::cout << *it << std::endl;

    //BreadthFirstSearch::cuda_bfs(gr, 0);

}

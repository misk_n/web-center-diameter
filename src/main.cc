//
// Created by Nicolas Misk on 5/19/18.
//

# include <dlfcn.h>
# include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

# include "Igraph.hpp"

factory_t factory;

static diameter::node_t read_graph(const std::string& filename, diameter::raw_arc_vector_t& edges)
{
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Unable to open file: " + filename << std::endl;
        exit(2);
    }

    std::string line;
    std::getline(file, line);

    auto numberOfNode = static_cast<diameter::node_t>(std::stoi(line));

    for (int i = 0; i < numberOfNode; ++i) {
        std::getline(file, line);
    }

    std::stringstream ss;
    std::string node1, node2;
    while (std::getline(file, line) && (ss << line)) {
        ss >> node1;
        ss >> node2;
        ss.clear();
        diameter::node_t from = std::stoll(node1);
        diameter::node_t to = std::stoll(node2);
        edges.emplace_back(from, to);
    }

    file.close();

    return numberOfNode;
}

static void reverse_graph(diameter::raw_arc_vector_t edges)
{
    // TODO: trivially parallel problem
    for (auto it = edges.begin(); it != edges.end(); it++)
    {
        auto tmp = it->first;
        it->first = it->second;
        it->second = tmp;
    }
}

namespace diameter
{
    node_t double_sweep(unsigned, diameter::Igraph&, diameter::Igraph&);
}

static diameter::node_t compute_exact_diameter(const std::vector<std::pair<diameter::node_t, diameter::node_t> >& order,
                                               std::vector<diameter::node_t>& dist,
                                               std::vector<diameter::node_t>& queue,
                                               std::vector<diameter::node_t>& ecc,
                                               diameter::Igraph& graph,
                                               diameter::Igraph& rgraph,
                                               diameter::node_t diameter,
                                               std::vector<diameter::node_t>& scc)
{
    for (int i = 0; i < graph.get_node_count(); i++) {
        int u = order[i].second;

        if (ecc[u] <= diameter) continue;

        // Refine the eccentricity upper bound
        diameter::node_t ub = 0;
        std::vector <std::pair<diameter::node_t, diameter::node_t> > neighbors;

        for (size_t j = 0; j < graph.get_adjacent(u).size(); j++)
            neighbors.push_back(std::make_pair(scc[graph.get_adjacent(u)[j]], ecc[graph.get_adjacent(u)[j]] + 1));

        std::sort(neighbors.begin(), neighbors.end());

        for (size_t j = 0; j < neighbors.size(); ) {
            diameter::node_t component = neighbors[j].first;
            diameter::node_t lb = graph.get_node_count();

            for (; j < neighbors.size(); j++) {
                if (neighbors[j].first != component) break;
                lb = std::min(lb, neighbors[j].second);
            }

            ub = std::max(ub, lb);

            if (ub > diameter) break;
        }

        if (ub <= diameter) {
            ecc[u] = ub;
            continue;
        }

        // Conduct a BFS and update bounds
        int qt = 0;

        auto res = graph.perform_bfs(u);

        ecc[u] = res.max_distance;
        diameter = std::max(diameter, ecc[u]);

        int qs;
        qs = qt = 0;
        dist[u] = 0;
        queue[qt++] = u;

        while (qs < qt) {
            diameter::node_t v = queue[qs++];

            ecc[v] = std::min(ecc[v], dist[v] + ecc[u]);

            for (size_t j = 0; j < rgraph.get_adjacent(v).size(); j++) {
                // only inside an SCC
                if (dist[rgraph.get_adjacent(v)[j]] < 0 && scc[rgraph.get_adjacent(v)[j]] == scc[u]) {
                    dist[rgraph.get_adjacent(v)[j]] = dist[v] + 1;
                    queue[qt++] = rgraph.get_adjacent(v)[j];
                }
            }
        }

    }

    return diameter;
}

static void order_vertices(std::vector<std::pair<diameter::node_t, diameter::node_t>>& order,
                           diameter::Igraph& graph,
                           diameter::Igraph& rgraph,
                           std::vector<diameter::node_t>& scc)
{
    for (int v = 0; v < graph.get_node_count(); v++) {
        int in = 0, out = 0;

        for (size_t i = 0; i < rgraph.get_adjacent(v).size(); i++) {
            if (scc[rgraph.get_adjacent(v)[i]] == scc[v]) in++;
        }

        for (size_t i = 0; i < graph.get_adjacent(v).size(); i++) {
            if (scc[graph.get_adjacent(v)[i]] == scc[v]) out++;
        }

        // SCC : reverse topological order
        // inside an SCC : decreasing order of the product of the indegree and outdegree for vertices in the same SCC
        order[v].first = (scc[v] << 32) - in * out;
        order[v].second = v;
    }

    std::sort(order.begin(), order.end());
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << std::string("Usage: ") + argv[0] + " [graph filename]" << std::endl;
        exit(1);
    }

    void *handle = dlopen("./libbfs.so", RTLD_NOW);
    if (handle == nullptr)
    {
        std::cerr << std::string("Impossible to open library libbfs.so: ") + dlerror() << std::endl;
        return 1;
    }

    std::cerr << "Step 0: Preparing" << std::endl;

    std::cerr << " - reading file:";
    diameter::raw_arc_vector_t edges;
    diameter::node_t nb_nodes = read_graph(argv[1], edges);
    std::cerr << " OK" << std::endl;

    std::cerr << " - create graph:";
    diameter::Igraph* graph = factory();
    graph->build(edges, nb_nodes);
    std::cerr << " OK" << std::endl;

    std::cerr << " - create reverse graph:";
    reverse_graph(edges);
    diameter::Igraph* rgraph = factory();
    rgraph->build(edges, nb_nodes);
    std::cerr << " OK" << std::endl;

    std::cerr << "Step 1: Double sweep" << std::endl;
    auto diameter = double_sweep(10, *graph, *rgraph); // TODO: choose nb double sweep wisely

    std::cerr << "Step 2: Compute Strongly Connected Components" << std::endl;
    auto V = graph->get_node_count();
    std::vector<diameter::node_t> scc(V, -1);
    graph->compute_scc(scc);

    std::cerr << "Step 3: Vertice ordering" << std::endl;
    std::vector<std::pair<diameter::node_t, diameter::node_t> > order(graph->get_node_count());
    order_vertices(order, *graph, *rgraph, scc);

    std::cerr << "Step 4: Compute exact diameter" << std::endl;
    std::vector<diameter::node_t> ecc(V, V);
    auto dist = std::vector<diameter::node_t>(V, -1);
    auto queue = std::vector<diameter::node_t>(V, -1);
    diameter = compute_exact_diameter(order, dist, queue,
                                      ecc, *graph, *rgraph, diameter, scc);

    std::cout << "Computed diameter: " + diameter << std::endl;

    delete graph;
    delete rgraph;

    return 0;
}

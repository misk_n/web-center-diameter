#!/bin/bash

nvcc --version > /dev/null 2> /dev/null

if [ $? == 0 ]; then
    ln -sf ./libgpu-bfs.so ./libbfs.so
else
    ln -sf ./libcpu-bfs.so ./libbfs.so
fi

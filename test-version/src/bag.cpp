//
// Created by Franck Thang on 29/05/2018.
//

#include "bag.h"


Node* pennant_union(Node* x, Node* y) {
    y->right_ = x->left_;
    x->left_ = y;
    return  x;
}

Bag::Bag(size_t size): size_(size) {
    array_ = new Node* [size];
    for (size_t k = 0; k < size; ++k) {
        array_[k] = nullptr;
    }
}

Bag *Bag::pennant_split() {
       return nullptr;
}

void Bag::bag_insert(Node * node) {
    size_t k = 0;
    while (array_[k] != NULL) {
        node = pennant_union(array_[k], node);
        array_[k++] = NULL;
    }

    array_[k] = node;
}

void Bag::  bag_union(Bag *other) {
    Node* carry = nullptr;
    for (size_t k = 0; k < size_; ++k) {
        Node* other_pennant = other->get_pennant(k);

        if (other_pennant != nullptr) {
            if (carry != nullptr) {
                carry = pennant_union(carry, other_pennant);
                continue;
            }

            if (array_[k] == nullptr) array_[k] = other_pennant;
            else {
                carry = pennant_union(array_[k], other_pennant);
                array_[k] = nullptr;
            }
        }

        else {
            if (carry != nullptr) {
                if (array_[k] == nullptr) {
                    array_[k] = carry;
                    carry = nullptr;
                }
                else {
                    carry = pennant_union(array_[k], carry);
                    array_[k] = nullptr;
                }
            }
        }
    }
}

bool Bag::is_empty() {
    for (size_t i = 0; i < size_; ++i) if (array_[i] != nullptr) return false;
    return true;
}

size_t Bag::get_size() {
    return size_;
}

Node *Bag::get_pennant(size_t k) {
    return k < size_ && k >= 0 ? array_[k] : nullptr;
}


Node* pennant_split(Node* x, Node* y) {
    y->right_ = x->left_;
    x->left_ = y;
    return  x;
}
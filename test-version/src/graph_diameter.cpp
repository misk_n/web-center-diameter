/*
  The MIT License (MIT)

  Copyright (c) 2015 Yuki Kawata

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <sstream>
# include "graph_diameter.h"
#include "bag.h"
#include <thread>
#include <mutex>
#include <omp.h>
#include <math.h>

//int GraphDiameter::bfs(int start,
//                       const std::vector<std::vector<int> >& bfs_graph,
//                       std::vector<int>& dist,
//                       std::vector<int>& queue)
//{
//  std::queue<int> q;
//  q.push(start);
//  q.push(-1);
//  int qt = 0;
//  dist[start] = 0;
//  queue[qt++] = start;
//
//  while (!q.empty()) {
//
//    timestamp_t t0 = get_timestamp();
//    while (q.front() != -1)
//    {
//      int v = q.front();
//      q.pop();
//
//      for (size_t j = 0; j < bfs_graph[v].size(); j++) {
//        if (dist[bfs_graph[v][j]] < 0) {
//          dist[bfs_graph[v][j]] = dist[v] + 1;
//
//
//          queue[qt++] = bfs_graph[v][j];
//          q.push(bfs_graph[v][j]);
//
//        }
//      }
//
//
//    }
//    timestamp_t t1 = get_timestamp();
//    parallelisable_secs += (t1 - t0) / 1000000.0L;
//
//    q.pop();
//      std::cout << qt << '\n';
//
//      if (!q.empty())
//          q.push(-1);
//
//
//  }
////    std::cout << qt << '\n';
//
//    throw "toto";
//  return qt;
//}



// Create this function to traverse the tree iteratively
void process_pennant_aux(Node *in_pennant,
                         Bag *out_bag,
                         const std::vector<std::vector<int> > &bfs_graph,
                         std::vector<int> &dist,
                         std::vector<int>& queue,
                         int& qt) {

    std::queue<Node*> queue_node;
    queue_node.push(in_pennant);

    while (!queue_node.empty()) {
        Node* pennant = queue_node.front();
        queue_node.pop();
        if (pennant->left_ != nullptr) queue_node.push(pennant->left_);
        if (pennant->right_ != nullptr) queue_node.push(pennant->right_);


        unsigned int nthread = bfs_graph[pennant->value_].size() > 200 ? 2 : 1;

        std::vector<Bag*> bag_reducer {out_bag};

        std::vector<int> counter_reducer{0};
        std::vector<std::vector<int>> node_reducer;
        std::vector<int> tmp(bfs_graph[pennant->value_].size());
        node_reducer.push_back(tmp);
        for (unsigned int i = 1; i < nthread; ++i) {
            bag_reducer.push_back(new Bag());
            counter_reducer.push_back(0);

            std::vector<int> tmp1(bfs_graph[pennant->value_].size());
            node_reducer.push_back(tmp1);
        }

        #pragma omp parallel for num_threads (nthread)
            for (size_t j = 0; j < bfs_graph[pennant->value_].size(); j++) {
                if (dist[bfs_graph[pennant->value_][j]] < 0) {
                    dist[bfs_graph[pennant->value_][j]] = dist[pennant->value_] + 1;

                    node_reducer[omp_get_thread_num()][counter_reducer[omp_get_thread_num()]++]
                            = bfs_graph[pennant->value_][j];

                    bag_reducer[omp_get_thread_num()]->bag_insert(
                            new Node((size_t) bfs_graph[pennant->value_][j], nullptr, nullptr));
                }
            }

        #pragma omp barrier
        for (unsigned int j = 0; j < counter_reducer[0]; ++j) {
            queue[qt++] = node_reducer[0][j];
        }

        for (unsigned int i = 1; i < nthread; ++i) {
            out_bag->bag_union(bag_reducer[i]);
            for (unsigned int j = 0; j < counter_reducer[i]; ++j) {
                queue[qt++] = node_reducer[i][j];
            }
        }
    }
}


void process_pennant(
        size_t in_pennant_size,
        Node *in_pennant,
        Bag *out_bag,
        const std::vector<std::vector<int> > &bfs_graph,
        std::vector<int> &dist,
        std::vector<int>& queue,
        int& qt
) {
    constexpr size_t grainsize = 7; // That means 2^7 in my case
    if (in_pennant_size < grainsize) {
        process_pennant_aux(in_pennant, out_bag, bfs_graph, dist, queue, qt);
    }

    else {
        Node* nw = in_pennant->split();
        // spawn
#pragma omp parallel sections
        {
#pragma omp section
            process_pennant(in_pennant_size / 2, nw, out_bag, bfs_graph, dist, queue, qt);
#pragma omp section
            process_pennant(in_pennant_size / 2, in_pennant, out_bag, bfs_graph, dist, queue, qt);
        }
#pragma omp barrier
        // sync
    }
}


void process_layer(
        Bag *in_bag,
        Bag *out_bag,
        const std::vector<std::vector<int> >& bfs_graph,
        std::vector<int>& dist,
        std::vector<int>& queue,
        int& qt)
{
    size_t bag_size = in_bag->get_size();

        for (size_t k = 0; k < bag_size; ++k) {
            Node* pennant = in_bag->get_pennant(k);

            if (pennant != nullptr) {
                process_pennant(k, pennant, out_bag, bfs_graph, dist, queue, qt);
            }

        }
}

void print_bag(Bag* in_bag) {
    size_t size = in_bag->get_size();
    std::cout << "Printing values ... \n";
    for (size_t k = 0; k < size; ++k) {
        if (in_bag->get_pennant(k) != nullptr) {
            Node* pennant = in_bag->get_pennant(k);

            std::queue<Node*> queue;
            queue.push(pennant);

            while (!queue.empty()) {
                Node* current_pennant = queue.front();
                queue.pop();
                if (current_pennant->left_ != nullptr) queue.push(current_pennant->left_);
                if (current_pennant->right_ != nullptr) queue.push(current_pennant->right_);

//                std::cout << current_pennant->value_ << '\n';
            }
        }
    }
}

long long int number_value(Bag* in_bag) {
    size_t size = in_bag->get_size();
    long long int ret = 0;
    for (size_t k = 0; k < size; ++k) {
        if (in_bag->get_pennant(k) != nullptr) {
            ret += pow(2, k);
        }
    }

    return ret;
}


int GraphDiameter::bfs(int start,
                       const std::vector<std::vector<int> >& bfs_graph,
                       std::vector<int>& dist,
                       std::vector<int>& queue)
{

    dist[start] = 0;

//    std::cout << "start: " << start << '\n';
    Bag *bag = new Bag();
    int qt = 0;
    queue[qt++] = start;

    bag->bag_insert(new Node(start, nullptr, nullptr));

    while (!bag->is_empty())
    {
//        std::cout << "Layer: " << layer << '\n';
        auto *new_bag = new Bag();
        process_layer(bag, new_bag, bfs_graph, dist, queue, qt);
        bag = new_bag;
    }
    return qt;
}


void GraphDiameter::compute_strongly_connected_component()
{
  int num_visit = 0, num_scc = 0;
  std::vector <int> ord(V, -1);
  std::vector <int> low(V);
  std::vector <bool> in(V, false);
  std::stack <int> s;
  std::stack <std::pair<int, int> > dfs;
        
  for (int i = 0; i < V; i++) {
    if (ord[i] != -1) continue;
            
    dfs.push(std::make_pair(i, -1));
            
    while (!dfs.empty()) {
      int v = dfs.top().first;
      int index = dfs.top().second;
                
      dfs.pop();
                
      if (index == -1) {
        ord[v] = low[v] = num_visit++;
        s.push(v);
        in[v] = true;
      } else {
        low[v] = std::min(low[v], low[graph[v][index]]);
      }
                
      for (index++; index < (int)graph[v].size(); index++) {
        int w = graph[v][index];
                    
        if (ord[w] == -1) {
          dfs.push(std::make_pair(v, index));
          dfs.push(std::make_pair(w, -1));
          break;
        } else if (in[w] == true) {
          low[v] = std::min(low[v], ord[w]);
        }
      }
                
      if (index == (int)graph[v].size() && low[v] == ord[v]) {
        while (true) {
          int w = s.top();
                        
          s.pop();
          in[w] = false;
          scc[w] = num_scc;
                        
          if (v == w) break;
        }
                    
        num_scc++;
      }
    }
  }
}

void GraphDiameter::order_vertices(std::vector<std::pair<long long, int>>& order)
{
  for (int v = 0; v < V; v++) {
    int in = 0, out = 0;
            
    for (size_t i = 0; i < rgraph[v].size(); i++) {
      if (scc[rgraph[v][i]] == scc[v]) in++;
    }
            
    for (size_t i = 0; i < graph[v].size(); i++) {
      if (scc[graph[v][i]] == scc[v]) out++;
    }
            
    // SCC : reverse topological order
    // inside an SCC : decreasing order of the product of the indegree and outdegree for vertices in the same SCC
    order[v] = std::make_pair(((long long)scc[v] << 32) - in * out, v);
  }
        
  std::sort(order.begin(), order.end());
}


void GraphDiameter::double_sweep(int num_double_sweep,
                                 std::vector<int>& dist,
                                 std::vector<int>& queue)
{
  int qt;
  // distribuable
  for (int i = 0; i < num_double_sweep; i++) {
    int start = GetRandom();
    // forward BFS
    qt = bfs(start, graph, dist, queue);

    // parallel
    for (int j = 0; j < qt; j++) dist[queue[j]] = -1;
            
    // barkward BFS
    qt = bfs(start, rgraph, dist, queue);
            
    diameter = std::max(diameter, dist[queue[qt - 1]]);

    // parallel
    for (int j = 0; j < qt; j++) dist[queue[j]] = -1;
  }
}

void GraphDiameter::compute_exact_diameter(const std::vector<std::pair<long long, int>>& order,
                                           std::vector<int>& dist,
                                           std::vector<int>& queue,
                                           std::vector<int>& ecc)
{
  for (int i = 0; i < V; i++) {
    int u = order[i].second;
            
    if (ecc[u] <= diameter) continue;
            
    // Refine the eccentricity upper bound
    int ub = 0;
    std::vector <std::pair<int, int> > neighbors;
            
    for (size_t j = 0; j < graph[u].size(); j++) neighbors.push_back(std::make_pair(scc[graph[u][j]], ecc[graph[u][j]] + 1));
            
    sort(neighbors.begin(), neighbors.end());
            
    for (size_t j = 0; j < neighbors.size(); ) {
      int component = neighbors[j].first;
      int lb = V;
                
      for (; j < neighbors.size(); j++) {
        if (neighbors[j].first != component) break;
        lb = std::min(lb, neighbors[j].second);
      }
                
      ub = std::max(ub, lb);
                
      if (ub > diameter) break;
    }
            
    if (ub <= diameter) {
      ecc[u] = ub;
      continue;
    }
            
    // Conduct a BFS and update bounds
    numBFS++;
    int qt = 0;

    qt = bfs(u, graph, dist, queue);

    ecc[u] = dist[queue[qt - 1]];
    diameter = std::max(diameter, ecc[u]);

    for (int j = 0; j < qt; j++) dist[queue[j]] = -1;

    int qs;
    qs = qt = 0;
    dist[u] = 0;
    queue[qt++] = u;

    while (qs < qt) {
      int v = queue[qs++];

      ecc[v] = std::min(ecc[v], dist[v] + ecc[u]);

      for (size_t j = 0; j < rgraph[v].size(); j++) {
        // only inside an SCC
        if (dist[rgraph[v][j]] < 0 && scc[rgraph[v][j]] == scc[u]) {
          dist[rgraph[v][j]] = dist[v] + 1;
          queue[qt++] = rgraph[v][j];
        }
      }
    }

    for (int j = 0; j < qt; j++) dist[queue[j]] = -1;
  }
}


int GraphDiameter::GetDiameter(const std::vector <std::pair<int, int> > &edges, int num_double_sweep) {
  // Prepare the adjacency list
  printf("Preparation\n");
  graph = std::vector<std::vector<int>>();
  rgraph = std::vector<std::vector<int>>();
  {
    for (size_t i = 0; i < edges.size(); i++) {
      int from = edges[i].first;
      int to = edges[i].second;
            
      V = std::max(V, from + 1);
      V = std::max(V, to + 1);
    }
        
    graph.resize(V);
    rgraph.resize(V);
        
    for (size_t i = 0; i < edges.size(); i++) {
      int from = edges[i].first;
      int to = edges[i].second;
            
      graph[from].push_back(to);
      rgraph[to].push_back(from);
    }
  }
    
  // Decompose the graph into strongly connected components
  printf("Strongly connected components decomposition\n");
  time = -GetTime();
  scc = std::vector<int>(V);
  compute_strongly_connected_component();

  // Compute the diameter lower bound by the double sweep algorithm
  printf("Lower bound by double sweep\n");
  std::vector <int> dist(V, -1);
  std::vector <int> queue(V);
  double_sweep(num_double_sweep,
               dist,
               queue);
    
  // Order vertices
    time += GetTime();
  printf("Vertices ordering\n");
  std::vector <std::pair<long long, int> > order(V);
  order_vertices(order);
    return diameter;
    
  // Examine every vertex
  printf("Final diameter computation\n");
  std::vector <int> ecc(V, V);
  compute_exact_diameter(order,
                         dist,
                         queue,
                         ecc);

  time += GetTime();

  return diameter;
}

static bool is_number(const std::string& s)
{
  return !s.empty() && std::find_if(s.begin(), 
                                    s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}

int GraphDiameter::GetDiameter(const char *filename, int num_double_sweep) {
    std::vector<std::pair<int,int>> edges;

    std::ifstream myfile(filename);
    if (!myfile.is_open()) {
        std::cerr << "Unable to open file\n";
        exit(1);
    }

    std::string line;
    std::getline(myfile, line);

    auto numberOfNode = static_cast<unsigned long>(std::stoi(line));

    for (int i = 0; i < numberOfNode; ++i) {
        std::getline(myfile, line);
    }

    std::stringstream ss;
    std::string node1, node2;
    while (std::getline(myfile, line) && (ss << line)) {
        ss >> node1;
        ss >> node2;
        ss.clear(); // Reset stringsteam
        int from = std::stoi(node1);
        int to = std::stoi(node2);
//        std::cout << from << " to " << to << '\n';
        edges.push_back(std::make_pair(from, to));
    }

    myfile.close();
    
  return GetDiameter(edges, num_double_sweep);
}

void GraphDiameter::PrintStatistics(void) {
  printf("Diameter : %d\n", diameter);
  printf("#BFS : %d -> %d\n", V, numBFS);
  printf("Time : %lf sec\n", time);
  printf("Parallelisable time : %lf sec\n", parallelisable_secs);
}

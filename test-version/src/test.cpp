#include <cstdio>
#include "../src/graph_diameter.h"
#include <ctime>
#include <omp.h>

int main(int argc, char *argv[]) {
  if (argc != 2) {
    fprintf(stderr, "Usage: test GRAPH\n");
    return 0;

  }

  GraphDiameter gd;
  std::clock_t start;
  double duration;
  start = std::clock();

  gd.GetDiameter(argv[1]);
  duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

  std::cout << "Duration: "<< duration <<'\n';

  gd.PrintStatistics();

  return 0;
}

/*
  The MIT License (MIT)

  Copyright (c) 2015 Yuki Kawata

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __GRAPH_DIAMETER_H__
#define __GRAPH_DIAMETER_H__

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stack>
#include <string>
#include <sys/time.h>
#include <vector>
#include <fstream>
#include <queue>

class GraphDiameter {
public:
    
GraphDiameter() : V(0), diameter(0), numBFS(0), time(0) {}
  ~GraphDiameter() {}
  int GetDiameter(const std::vector <std::pair<int, int> > &edges, int num_double_sweep = numDefaultDoubleSweep);
  int GetDiameter(const char *filename, int num_double_sweep = numDefaultDoubleSweep);
  void PrintStatistics(void);
    
private:
    
  static const int numDefaultDoubleSweep = 10;
  int V;
  int diameter;
  int numBFS;
  double time;
  double parallelisable_secs = 0;
  std::vector<std::vector<int>> graph;
  std::vector<std::vector<int>> rgraph;
  std::vector<int> scc;

  typedef unsigned long long timestamp_t;

  static timestamp_t
    get_timestamp ()
  {
    struct timeval now;
    gettimeofday (&now, NULL);
    return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
  }

  double GetTime(void) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + tv.tv_usec * 1e-6;
  }
    
  int GetRandom(void) {
    static unsigned x = 123456789;
    static unsigned y = 362436039;
    static unsigned z = 521288629;
    static unsigned w = 88675123;
    unsigned t;
        
    t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
        
    return w % V;
  }

  int bfs(int start,
          const std::vector<std::vector<int> >& bfs_graph,
          std::vector<int>& dist,
          std::vector<int>& queue);

  void compute_strongly_connected_component();

  void order_vertices(std::vector<std::pair<long long, int>>& order);

  void double_sweep(int num_double_sweep,
                    std::vector<int>& dist,
                    std::vector<int>& queue);

  void compute_exact_diameter(const std::vector<std::pair<long long, int>>& order,
                              std::vector<int>& dist,
                              std::vector<int>& queue,
                              std::vector<int>& ecc);

};

#endif

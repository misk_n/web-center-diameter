# include <vector>

namespace diameter {

    using node_t = long long int;
    using raw_arc_vector_t = std::vector<std::pair<node_t, node_t>>;

    struct bfs_status
    {
        std::vector<node_t> last_frontier;
        node_t max_distance;
    };

    class Igraph {
    public:
        virtual struct bfs_status perform_bfs(node_t start) = 0;

        virtual void build(raw_arc_vector_t &, node_t) = 0;

        virtual node_t get_node_count() const = 0;

        virtual void compute_scc(std::vector<node_t>& scc) = 0;

        virtual std::vector<node_t> get_adjacent(node_t n) = 0;
    };
}

using factory_t = diameter::Igraph *(*)();
extern factory_t factory;

namespace diameter
{

    /*
     * Base class for every graph
     */

    class graph_base
    { };

    /*
     * Template class to allow code factorisation between GPU and CPU implementations
     * GPU and CPU graphs are specialization of this class, that implement
     * the libgrah's Igraph interface.
     */

    // TODO: investigate template template syntax
    template<typename node_t,
            typename node_vector_t,
            typename raw_arc_vector>
    class graph : public graph_base
    {
    protected:
        using node = node_t;

        void build(raw_arc_vector& arcs, node_t nb_nodes);

        node_vector_t& get_destinations();

        node_vector_t& get_sources();

        node_t get_nb_nodes() const;

        std::vector<node_t> get_adjacent(node_t n);

    private:
        node_vector_t destinations_;
        node_vector_t sources_;
        node_t nb_nodes_;
    };


}

# include "graph.hxx"

namespace diameter
{

    // protected interface:

    template<typename node_t,
            typename node_vector_t,
            typename raw_arc_vector>
    void graph<node_t, node_vector_t, raw_arc_vector>::
    build(raw_arc_vector& arcs, node_t nb_nodes)
    {
        node_t j = 0;
        for (node_t i = 0; i < nb_nodes; i++)
        {
            if (arcs[j].first != i)
            {
                sources_.resize(i + 1);
                sources_[i] = 0;
            }
            else
            {
                do
                {
                    destinations_.push_back(arcs[j].second);
                    sources_.resize(i + 1);
                    sources_[i]++;
                    j++;
                } while (arcs[j].first == i);
            }
        }
        nb_nodes_ = nb_nodes;
        for (node_t i = 1; i < sources_.size(); i++)
        {
            sources_[i] += sources_[i - 1];
        }
    }

    template<typename node_t,
            typename node_vector_t,
            typename raw_arc_vector>
    node_vector_t& graph<node_t, node_vector_t, raw_arc_vector>::
    get_destinations()
    {
        return destinations_;
    }

    template<typename node_t,
            typename node_vector_t,
            typename raw_arc_vector>
    node_vector_t& graph<node_t, node_vector_t, raw_arc_vector>::
    get_sources()
    {
        return sources_;
    }

    template<typename node_t,
            typename node_vector_t,
            typename raw_arc_vector>
    node_t
    graph<node_t, node_vector_t, raw_arc_vector>::
    get_nb_nodes() const
    {
        return nb_nodes_;
    }

    template<typename node_t,
            typename node_vector_t,
            typename raw_arc_vector>
    std::vector<node_t>
    graph<node_t, node_vector_t, raw_arc_vector>::
    get_adjacent(node_t n)
    {
        std::vector<node_t> res;
        for (auto i = n; i < sources_[n]; i++)
            res.push_back(destinations_[i]);
        return res;
    }

}

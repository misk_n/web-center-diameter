#!/bin/bash

if ! [ -a "webbase-2001/webbase-2001.mtx" ]; then
    wget -O https://sparse.tamu.edu/MM/LAW/webbase-2001.tar.gz
    tar -xf --checkpoint=100 webbase-2001.tar.gz
else
    echo "File already downloaded and extracted"
fi

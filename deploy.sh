#/bin/bash

if [ $# != 3 ]; then
    echo "Usage: $0 [hostfile] [file] [user]" >&2
    exit 1
fi

cat $1 | {
    while read host; do
        cmd="scp $2 $3@${host}:~"
        $cmd
    done
}
